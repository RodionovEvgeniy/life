//
//  CustomTableViewCell.swift
//  Life
//
//  Created by Евгений Родионов on 05/05/2019.
//  Copyright © 2019 Евгений Родионов. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    
    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var checkBtn: CheckBox!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    @IBAction func touchCheckbox(_ sender: CheckBox) {
        checkBtn.isChecked = !checkBtn.isChecked
        backgroundColor = checkBtn.isChecked ? UIColor.green : UIColor.white
    }
    
}
