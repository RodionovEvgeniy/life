//
//  Category.swift
//  Life
//
//  Created by Евгений Родионов on 04/05/2019.
//  Copyright © 2019 Евгений Родионов. All rights reserved.
//

import Foundation

struct Category: Decodable {
    var name: String
    var code: Int
    
}
