//
//  CategoryHeaderView.swift
//  Life
//
//  Created by Евгений Родионов on 07/05/2019.
//  Copyright © 2019 Евгений Родионов. All rights reserved.
//

import UIKit

protocol CategoryHeaderViewDelegate{
    func simpleDidTap(header: CategoryHeaderView) //передаем VC код категории т е идентификатор хедера
}

class CategoryHeaderView: UITableViewHeaderFooterView {

    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var simpleButton: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    var categoryCode: Int!
    var delegate: CategoryHeaderViewDelegate?
    var isExpanded = true
    
    @IBAction func simpleButtonAction(_ sender: UIButton) {
       // isExpanded = !isExpanded
        delegate?.simpleDidTap(header: self)
    }
    
}
