//
//  CheckBox.swift
//  Life
//
//  Created by Евгений Родионов on 19/05/2019.
//  Copyright © 2019 Евгений Родионов. All rights reserved.
//

import UIKit

class CheckBox: UIButton {
   
    private let checkedImage = UIImage(named: "unchecked-checkbox-filled")! as UIImage
    private let uncheckedImage = UIImage(named: "check-box-3")! as UIImage
    
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(uncheckedImage, for: .normal)
            } else {
                self.setImage(checkedImage, for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
//        addTarget(self, action: #selector(btnBox(_:)) , for: .touchUpInside)
        //self.isChecked = false
        
    }
    
//    @objc func btnBox(_ sender: UIButton) {
//        if sender == self {
//            isChecked = !isChecked
//        }
//    }


}
