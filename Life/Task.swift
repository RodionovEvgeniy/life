//
//  Task.swift
//  Life
//
//  Created by Евгений Родионов on 04/05/2019.
//  Copyright © 2019 Евгений Родионов. All rights reserved.
//

import Foundation

class Task {
    let name: String
    let isDone: Bool = false
    let categoryCode: Int
    let countTask: Int
    
    init(name: String, categoryCode: Int, countTask: Int) {
        self.name = name
        //self.isDone = false
        self.categoryCode = categoryCode
        self.countTask = countTask
    }
}


extension Task: Equatable {
    static func == (lhs:Task , rhs: Task) -> Bool {
        return lhs.name == rhs.name
    }
}
