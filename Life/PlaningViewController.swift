//
//  PlaningViewController.swift
//  Life
//
//  Created by Евгений Родионов on 04/05/2019.
//  Copyright © 2019 Евгений Родионов. All rights reserved.
//

import UIKit

class PlaningViewController: UIViewController {
    @IBOutlet weak var categoriesTextField: UITextField!
    
    @IBOutlet weak var taskTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var categories = [(category: Category, isExpanded: Bool)]()
    var currentCategory: Category?
    var tasks = [Task]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        let nib = UINib(nibName: "CategoryHeaderView", bundle: Bundle.main)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "Category Header")
        let path = Bundle.main.path(forResource: "Categories", ofType: "json")!
        let url = URL(fileURLWithPath: path)
        do {
             let data = try Data(contentsOf: url)
             let cat = try JSONDecoder().decode([Category].self, from: data)
            categories = cat.map({ (name: $0, isExpanded: true)})
        } catch {
            print("111")
        }
        
    

    }
    
    @IBAction func addTask(_ sender: UIButton) {
        guard let taskName = taskTextField.text, let categoryCode = currentCategory?.code else {
                return }
        let task = Task(name: taskName, categoryCode: categoryCode, countTask: tasks.count )
        tasks.append(task)
        taskTextField.resignFirstResponder()
        DispatchQueue.main.async {
                let section = self.categories.lastIndex(where: { task.categoryCode == $0.category.code})!
                self.tableView.reloadSections(IndexSet(arrayLiteral: section), with: .automatic)
        }

        
    }
    
    
    @IBAction func down(_ sender: UITextField) {
        let alert = UIAlertController(title: "Choose category", message: nil, preferredStyle: .alert)
       //alert.addTextField()
        alert.isModalInPopover = true
        let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        alert.addAction(cancel)
        alert.view.translatesAutoresizingMaskIntoConstraints = false
        let picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: alert.view.frame.width, height: 200))
        alert.view.addSubview(picker)
        picker.translatesAutoresizingMaskIntoConstraints = false
        picker.centerXAnchor.constraint(equalTo: alert.view.centerXAnchor).isActive = true
        picker.centerYAnchor.constraint(equalTo: alert.view.centerYAnchor).isActive = true
        alert.view.heightAnchor.constraint(equalToConstant: 300).isActive = true
        picker.dataSource = self
        picker.delegate = self
        let ok = UIAlertAction(title: "ok", style: .default) { (_) in
            let row = picker.selectedRow(inComponent: 0)
            self.currentCategory = self.categories[row].category
            self.categoriesTextField.text = self.currentCategory?.name
        }
        alert.addAction(ok)
        
        alert.popoverPresentationController?.sourceView = sender
      
        
        present(alert, animated: true, completion: nil)
    }
    
    

}

extension PlaningViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row].category.name
    }
    
}

extension PlaningViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let category = categories[section]
        return category.isExpanded ? tasks.filter({ $0.categoryCode == category.category.code}).count : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "Category Header") as? CategoryHeaderView {
            header.categoryLabel.text = categories[section].category.name
            header.categoryCode = categories[section].category.code
            header.isExpanded = categories[section].isExpanded
            header.tag = section
            let code = header.categoryCode
            let filteredTasks = tasks.filter { (task) -> Bool in
                return task.categoryCode == code ? true : false
            }
            //let fil = tasks.filter{$0.categoryCode == code}
            
            header.simpleButton.setTitle(String(filteredTasks.count), for: .normal)
            header.delegate = self
            return header
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Task Cell", for: indexPath) as! CustomTableViewCell
        let section = indexPath.section
        if let header = tableView.headerView(forSection: section) as? CategoryHeaderView {
            let categoryCode = header.categoryCode
            let tas = tasks.filter{$0.categoryCode == categoryCode}
            cell.taskName.text = tas[indexPath.row].name
            cell.checkBtn.isChecked = tas[indexPath.row].isDone
            
//            let tc = String(tas.count)
//            let ssa = CategoryHeaderView()
//            if tc != nil {
//                ssa.simpleButton?.setTitle(tc, for: .normal)
//            }
        }
//        let checkBox = CheckBox() // выделение строки зеленым цветом
//        if checkBox.isChecked == false{
//            let cell = tableView.cellForRow(at: indexPath)
//            cell?.backgroundColor = .green
//        }
        
        
       // cell.textLabel?.text = categories[indexPath.row].name
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            let categoryHeader = tableView.headerView(forSection: indexPath.section) as! CategoryHeaderView
            let _tasks = tasks.filter{$0.categoryCode == categoryHeader.categoryCode}
            let taskToRemove = _tasks[indexPath.row]
            
            tasks.removeAll { (task: Task) -> Bool in
                return taskToRemove === task
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadSections(IndexSet(arrayLiteral: indexPath.section), with: .automatic)
            }
        }
    }
    
    
}

//
extension PlaningViewController: CategoryHeaderViewDelegate{
    func simpleDidTap(header: CategoryHeaderView) {
        let section = header.tag
        categories[section].isExpanded = !categories[section].isExpanded
        DispatchQueue.main.async {
            self.tableView.reloadSections([section], with: .fade) // индекс секции по коду
        }
        
    }


}
